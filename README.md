# Starter Code for Mobile + Particle.io Apps
---

This repo contains the starter code for making Android or IOS (Swift) projects that talk to a Particle.io Photon chip.

## System requirements:

* Android: Java 8
* IOS:  XCode 9+

## How to use

1. Download source code
2. Choose either Android or IOS
3. For Android projects, open directly in `Android Studio`
4. For IOS projects, open the `.xcworkspace` folder in `XCode`

## Connecting to Photon

In the code, add your Particle

* username
* password
* device_id

Press `PLAY`

Check the `Terminal`.  App will show success message if it was able to connect to the Particle Cloud.
